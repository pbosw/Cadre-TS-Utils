# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog]
and this project adheres to [Semantic Versioning].

## [1.0.5] - 2018-28-10
### Fixed
- First fully working publication with no erroneous artifacts

[1.0.5]: https://github.com/siggame/Cadre-TS-Utils/releases/tag/v1.0.5

[Keep a Changelog]: http://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: http://semver.org/spec/v2.0.0.html
