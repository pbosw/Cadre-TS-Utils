/** A simple 2D point. */
export interface IPoint {
    /** The X-Coordinate. */
    x: number;

    /** The Y-Coordinate. */
    y: number;
}
