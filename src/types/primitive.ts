/** Primitive types in JS that are not containers. */
export type Primitive
    = undefined
    | null
    | boolean
    | string
    | number
    | Function; // tslint:disable-line:ban-types
